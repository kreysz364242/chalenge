import React from 'react'
import Header from '../../partsOfComponents/Header/Header'
import Footer from '../../partsOfComponents/Footer/Footer'


const Home: React.FC = () =>{
    return(
        <div className='page-container'>
            <Header/>
            <Footer/>
        </div>
    )
}

export default Home