import React from 'react'
import { NavLink } from 'react-router-dom'
import facebook_blue from '../../styles/img/facebook-blue.svg'
import facebook_white from '../../styles/img/instagram-blue.svg'
import './Footer.scss'

const Footer: React.FC = () => {
    return (
        <div className='footer container-fluid'>
            <div className='footer__content'>
                <div className='footer__links'>
                    <NavLink className='footer__link' to='/'>Home</NavLink>
                    <NavLink className='footer__link' to='/'>Terms and conditions</NavLink>
                    <NavLink className='footer__link' to='/'>Privacy Policy</NavLink>
                    <NavLink className='footer__link' to='/'>Collection Statement</NavLink>
                    <NavLink className='footer__link' to='/'>Help</NavLink>
                    <NavLink className='footer__link' to='/'>Manage Account</NavLink>
                </div>
                <p className="footer__demo">Copyright © 2016 DEMO Streaming. All Rights Reserved.</p>
                <div className="footer__images">
                    <svg className="icon">
                        <use xlinkHref={`${facebook_blue}#svg`} />
                    </svg>
                    <svg className="filters-filter-prefix">
                        <use xlinkHref={`${facebook_blue}`} />
                    </svg>
                </div>
            </div>
        </div>
    )
}

export default Footer