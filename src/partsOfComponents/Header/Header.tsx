import React from 'react'
import './Header.scss'
import { NavLink } from 'react-router-dom'


const Header: React.FC = () => {
    return (
        <div className='header'>
            <div className='header-menu'>
                <div className='header-menu__items content-container row justify-content-between mt-auto'>
                    <p className='header-menu__name col-3'>DEMO Streaming</p>
                    <div className='header-menu__buttons col-4 row'>
                        <div className='header-menu__link item-container-link col-3'>
                            <NavLink className='header-menu__link' to='/'>Log in</NavLink>
                        </div>
                        <div className='header-menu__button item-container-button col-9'>
                            <button className='header-menu__button'>Start your free trial</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className='header-titles'>
                <div className='header-titles__items content-container'>
                    <div className='header-titles__titles'>Popular titles</div>
                </div>
            </div>
        </div>
    )
}

export default Header