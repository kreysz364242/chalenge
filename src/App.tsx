import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './components/Home/Home'
import './styles/global.scss'

const App =()=>{
    return(

        <Router>
            <Switch>
                <Route exact path='/' component={Home}/>
                {/* // <Route exact path='/' component={Series}/>
                // <Route exact path='/' component={Movies}/>  */}
            </Switch>
        </Router>
    )
}

export default App